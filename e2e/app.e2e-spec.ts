import { AngularPhPage } from './app.po';

describe('angular-ph App', function() {
  let page: AngularPhPage;

  beforeEach(() => {
    page = new AngularPhPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
